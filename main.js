/**
 * Created by weekee on 28/3/16.
 */
var express = require("express");
var app = express();

app.get("/",
    function(req, res) {

        //Status
        res.status(202);
        //MIME type
        res.type("text/plain")
        res.send("The current time is " + (new Date()));
}
);

app.get("/hello",
    function(req, res) {

        //Status
        res.status(202);
        //MIME type
        res.type("text/html")
        res.send("<h1>HELLO</h1>");
    }
);

//3000 - port number
app.listen(3000, function() {
    console.info("Application started on port 3000")
});